#ifndef COORDONNEE_H
#define COORDONNEE_H
/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *                                                       *
 *  Eric Beaudry - beaudry.eric@uqam.ca - ericbeaudry.ca */

//#if !defined(_COORDONNEE__H_)
//#define _COORDONNEE__H_
#include <iostream>

class Coordonnee {
  public:
    Coordonnee(){}
    Coordonnee(double latitude_, double longitude_);
    Coordonnee(const Coordonnee&);
    double distance(const Coordonnee&) const;
  private:
    double latitude;
    double longitude;

  friend std::ostream& operator << (std::ostream&, const Coordonnee&);
  friend std::istream& operator >> (std::istream&, Coordonnee&);
};
#endif // COORDONNEE_H

