#include "lieux.h"

Tableau<Site> Lieux::getAppartements()
{
  return appartements;
}

Tableau<Site> Lieux::getSites()
{
  return sites;
}

Tableau<Site> Lieux::obtenirMeilleursSites(double maxDistance)
{
  int nombreMeilleursSites = 0;
  int taille = appartements.taille();
  Tableau<Site> meilleursSites;
  for (int i = 0; i < taille; i++)
  {
    int nombreSitesTmp = appartements[i].nombreSiteProximite(sites, maxDistance);

    // Si le nombre de sites près est supérieur au nombre courant,
    // on vide le tableau et met à jour les informations.
    if (nombreSitesTmp > nombreMeilleursSites)
    {
      meilleursSites.vider();
      meilleursSites.ajouter(appartements[i]);
      nombreMeilleursSites = nombreSitesTmp;
    }
    else if (nombreSitesTmp == nombreMeilleursSites)
    { // Si le nombre de sites près est égal au nombre courant, on ajoute le site au tableau.
      meilleursSites.ajouter(appartements[i]);
    }
  }
  return meilleursSites;
}

std::istream& operator >>(std::istream& is, Lieux& l)
{
  // À compléter : ce code ne fait que lire le fichier.
  // Vous devez le compléter pour assigner les valeurs lues dans les attributs de Lieux.

  Site site;

  while (!is.eof())
  {
    is >> site;
    if (site.getNom().compare("imm") == 0)
    {
      l.appartements.ajouter(site);
    }
    else
    {
      l.sites.ajouter(site);
    }
  }

  return is;
}
