#if !defined(__LIEUX_H__)
#define __LIEUX_H__

#include <iostream>
#include <string>
#include "coordonnee.h"
#include "site.h"
#include "tableau.h"

using namespace std;

class Lieux{
  public:

    /**
    * @return un tableau d'appartements.
    */
    Tableau<Site> getAppartements();

    /**
     * @return un tableau de sites.
     */
    Tableau<Site> getSites();

    /**
     * Trouve parmi les appartements ceux ayant le plus sites à proximité.
     *
     * @param maxDistance la distance maximale pour qu'un lieu soit considéré à proximité d'un site.
     * @return un tableau de site qui contient les meilleurs sites.
     */
    Tableau<Site> obtenirMeilleursSites(double maxDistance);

  private:

    Tableau<Site> sites; //Sites à considérer
    Tableau<Site> appartements; //Appartements à considérer

    friend std::istream& operator >>(std::istream&, Lieux& l);
};
#endif // LIEUX_H

