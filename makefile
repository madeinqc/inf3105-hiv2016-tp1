# Makefile pour INF3105 / TP1

#OPTIONS = -Wall           # option standard
#OPTIONS = -g -O0 -Wall    # pour rouler dans gdb
OPTIONS = -O2 -Wall -std=c++0x       # pour optimiser

#
all : tp1

tp1 : tp1.cpp coordonnee.h coordonnee.o site.h site.o lieux.h lieux.o 
	g++ ${OPTIONS} -o tp1 tp1.cpp coordonnee.o site.o lieux.o 

coordonnee.o : coordonnee.cpp coordonnee.h
	g++ ${OPTIONS} -c -o coordonnee.o coordonnee.cpp

site.o : site.cpp site.h 
	g++ ${OPTIONS} -c -o site.o site.cpp

lieux.o : lieux.cpp lieux.h 
	g++ ${OPTIONS} -c -o lieux.o lieux.cpp

clean:
	rm -rf tp1 *~ *.o

