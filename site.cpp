#include "site.h"
Site::Site(string pNom, Coordonnee pAdresse)
{
    nom = pNom;
    adresse = pAdresse;
}

int Site::nombreSiteProximite(Tableau<Site> sites, double distMax) {
    int compteur = 0;
    int nbSites = sites.taille();
    for (int i = 0; i < nbSites; i++)
    {
        if (adresse.distance(sites[i].getAdresse()) <= distMax)
        {
            compteur++;
        }
    }
    return compteur;
}

std::istream& operator >> (std::istream& is, Site& site)
{
    is >> site.nom >> site.adresse;
    return is;
}

std::ostream& operator << (std::ostream& os, Site& site)
{
    os << site.nom
    << " "
    << site.adresse;

    return os;
}
