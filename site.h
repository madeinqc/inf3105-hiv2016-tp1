#ifndef SITE_H
#define SITE_H


#include <iostream>
#include <string>
#include "coordonnee.h"
#include "tableau.h"


using namespace std;

class Site{
public:
    Site(){}
    Site(string nom, Coordonnee adresse);

    /**
     * Trouve le nombre de fichiers près d'un site
     *
     * @param sites un tableau de sites à vérifier s'ils sont près de l'immeuble
     * @param distMax la distance maximale pour qu'un site soit considéré proche de l'immeuble
     */
    int nombreSiteProximite(Tableau<Site> sites, double distMax);

    const string& getNom() const{return nom;}
    const Coordonnee& getAdresse() const { return adresse;}

private:
    string nom;
    Coordonnee adresse;

    friend std::ostream& operator << (std::ostream& , Site &);
    friend std::istream& operator >> (std::istream& , Site & );
};
#endif // SITE_H

