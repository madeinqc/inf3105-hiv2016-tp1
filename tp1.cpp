/*    UQAM / Département d'informatique                          *
 *    INF3105 - Structures de données et algorithmes             *
 *    Hiver 2016 / TP1                                           *
 *    AUTEUR(S): Marc-Antoine Sauvé - SAUM13119008               *
 *               Nicolas Lamoureux - LAMN19109003                */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "tableau.h"
#include "lieux.h"

#define MAX_DISTANCE 3000
using namespace std;

/**
 * Vérifie que le fichier est valide.
 *
 * @param le fichier à vérifier
 * @return false si le fichier est invalide,
 *         true si le fichier est valide.
 */
bool verifFichier(ifstream& is)
{
    is.get();
    if (is.eof() || is.fail())
    {
        return false;
    }
    is.unget();

    return true;
}

/**
 * Avec le fichier en paramètre, trie les lieux et affiche les meilleurs sites.
 *
 * @param le fichier à traiter
 */
void trouverMeilleursSites(ifstream& is)
{
    Lieux lieux;

    while (is) // On ajoute tous les lieux au tableau
    {
        is >> lieux;
    }

    cout << "Candidats ideals :" << endl;

    Tableau<Site> meilleursImm = lieux.obtenirMeilleursSites(MAX_DISTANCE);
    int nbImm = meilleursImm.taille();
    Tableau<Site> sites = lieux.getSites();
    int nbSitesPres = meilleursImm[0].nombreSiteProximite(sites, MAX_DISTANCE);

    for (int i = 0; i < nbImm; i++)
    {
        cout << meilleursImm[i] << endl;
        cout << "Nombre de sites accessibles: " << nbSitesPres << endl;
    }
}

int main(int argc, const char** argv)
{
    if (argc != 2) // Si le nombre de paramètres n'est pas bon, on affiche une erreur.
    {
        cout << "Veuillez fournir le chemin d'un fichier en argument." << endl;
        return -1;
    }

    ifstream fichierQuartiers(argv[1]);

    if (verifFichier(fichierQuartiers)) // Si le fichier est valide, on cherche les meilleurs sites.
    {
        trouverMeilleursSites(fichierQuartiers);
    }
    else
    {
        cout << "Veuillez fournir un fichier valide avec des données." << endl;
        return -1;
    }

    return 0; // fin normale
}
